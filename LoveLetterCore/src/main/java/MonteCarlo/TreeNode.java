/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MonteCarlo;

import GameStates.GameMove;
import GameStates.GameState;
import ammonclegg.lovelettercore.Player;
import java.util.List;

/**
 *
 * @author ammon
 */
public interface TreeNode {
    public GameMove getMove();
    public TreeNode addChild(GameMove move, Player justMoved);
    public TreeNode selectChild(List<GameMove> legalMoves, double exploration);
    public TreeNode getParent();
    public void update(GameState terminalState);
    public Player playerWhoJustMoved();
    public List<GameMove> getUntriedMoves(List<GameMove> legalMoves);
//    public boolean hasChildren();
    public GameMove selectUntriedMove(List<GameMove> possibleMoves);
    public GameMove getBestMove();
    
    public String treeToString(int indent);
    public String indentString(int indent);
}
