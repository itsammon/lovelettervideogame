/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MonteCarlo;

import GameStates.GameMove;
import GameStates.GameState;
import ammonclegg.lovelettercore.Player;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author ammon
 */
public class ISTreeNode implements TreeNode{
    private GameMove move;
    private ISTreeNode parent;
    private List<ISTreeNode> children;
    public int wins = 0;
    public int visits = 0;
    // Availability of nodes
    public int avails = 1;
    private Player playerJustMoved;
    
    // Default for RootNodes
    public ISTreeNode()
    {
        this.move = null;
        this.parent = null;
        children = new ArrayList<>();
        this.playerJustMoved = null;
    }

    public ISTreeNode(GameMove move, ISTreeNode parent, Player playerJustMoved)
    {
        this.move = move;
        this.parent = parent;
        children = new ArrayList<>();
        this.playerJustMoved = playerJustMoved;
    }
    
    @Override
    public GameMove getMove() {
        return move;
    }

    @Override
    public TreeNode addChild(GameMove move, Player justMoved) {
        ISTreeNode node = new ISTreeNode(move, this, justMoved);
        children.add(node);
        return node;
    }

    @Override
    public TreeNode selectChild(List<GameMove> legalMoves, double exploration) {
        // Implements the UCB1 formula to get the child node
        List<ISTreeNode> legalChildren = new ArrayList<>();
        double maxUCBScore = -1.0;
        ISTreeNode bestChild = null;
        
        for (ISTreeNode child: children)
        {
            if (legalMoves.contains(child.getMove()))
                legalChildren.add(child);
        }
        
        // Get the best child to test
        for (ISTreeNode child : legalChildren) {
            if (ucbScore(child, exploration) > maxUCBScore) {
                maxUCBScore = ucbScore(child, exploration);
                bestChild = child;
            }
            // Update availabitity counts easier to do now instead of backpropagation time
            child.avails += 1;
        }
        return bestChild;
    }
    
    // Calculate the ucbScore for a node
    private double ucbScore(ISTreeNode node, double exploration)
    {
        float score = (float)node.wins/(float)node.visits;
        score += exploration * Math.sqrt(Math.log(node.avails)/(float)node.visits);
        return score;
    }

    @Override
    public TreeNode getParent() {
        return parent;
    }

    @Override
    public void update(GameState terminalState) {
        this.visits += 1;
        if (playerJustMoved != null)
            wins += terminalState.getResult(playerJustMoved);
    }

    @Override
    public Player playerWhoJustMoved() {
        return playerJustMoved;
    }

    @Override
    public List<GameMove> getUntriedMoves(List<GameMove> legalMoves) {
        List<GameMove> untriedMoves = new ArrayList<>();
        
        // Get the list of tried moves
        List<GameMove> triedMoves = new ArrayList<>();
        for (TreeNode child: children)
        {
            triedMoves.add(child.getMove());
        }
        
        // Add moves to untried if we don't have children for them
        for (GameMove move: legalMoves)
        {
            if (!triedMoves.contains(move))
            {
                untriedMoves.add(move);
            }
        }
        return untriedMoves;
    }

    @Override
    public GameMove selectUntriedMove(List<GameMove> untriedMoves) {
        // Choose a move at random from the list of untried moves
        return untriedMoves.get(ThreadLocalRandom.current().nextInt(0, untriedMoves.size()));
    }

    @Override
    public GameMove getBestMove() {
        int visits = -1;
        ISTreeNode mostVisited = null;
        for (ISTreeNode child: children)
        {
            if (child.visits > visits)
            {
                visits = child.visits;
                mostVisited = child;
            }
        }
        if (mostVisited != null)
        {
            return mostVisited.getMove();
        }
        else
        {
            // Decide what to do if no node was most visited
            return null;
        }
    }
    
    @Override
    public String toString()
    {
        String s = "W/V/A: " + wins + "/" + visits + "/" + avails;
        s += "M: ";
        return s;
    }
    
    @Override
    public String treeToString(int indent)
    {
        String s;
        s = indentString(indent) + toString();
        if (move != null)
        {
            s += move.getCard().getName() + " O: ";
            s += move.getOwner() + " T: " + move.getTarget() + " G: " + move.getGuess();
        }
        for (ISTreeNode child: children)
        {
            s += child.treeToString(indent+1);
        }
        return s;
    }
    
    @Override
    public String indentString(int indent)
    {
        String s = "\n";
        for (int i = 0; i < indent; ++i)
        {
            s += "| ";
        }
        return s;
    }
}
