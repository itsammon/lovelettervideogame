/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MonteCarlo;

import GameStates.GameState;

/**
 *
 * @author ammon
 */
public interface TreeNodeCreator {
    public TreeNode genRootNode(GameState state);
}
