/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameStates;

import ammonclegg.lovelettercore.Card;
import java.util.Objects;

/**
 *
 * @author ammon
 */
public class LoveLetterMove implements GameMove{
    private Card card;
    private int target;
    private int guess;
    private int owner;
    
    public LoveLetterMove(Card c, int target, int guess, int owner)
    {
        card = c;
        this.target = target;
        this.guess = guess;
        this.owner = owner;
    }
    
    @Override
    public Card getCard() {
        return card;
    }

    @Override
    public int getTarget() {
        return target;
    }

    @Override
    public int getGuess() {
        return guess;
    }

    @Override
    public int getOwner() {
        return owner;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if ( !(obj instanceof LoveLetterMove))
            return false;
        
        LoveLetterMove other = (LoveLetterMove) obj;
        
        if ((this.card == null) || (other.card == null))
            return false;
        if (!this.card.equals(other.card)) {
            return false;
        }
        if (this.target != other.target) {
            return false;
        }
        if (this.guess != other.guess) {
            return false;
        }
        return this.owner == other.owner;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.card);
        hash = 17 * hash + this.target;
        hash = 17 * hash + this.guess;
        hash = 17 * hash + this.owner;
        return hash;
    }
}
