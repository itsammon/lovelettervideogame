/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameStates;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.Deck;
import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.Player;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class LoveLetterGameState extends GameState{

    // Used to get the players starting hands/players
    private List<Player> players;
    private Deck deck;
    private List<Card> totalCards;
    private List<Card> drawPile = new ArrayList<>();
    private List<Card> extraRevealed = new ArrayList<>();
    private Map<Player, List<Card>> playersHands = new HashMap<>();
    private Map<Player, List<Card>> playersDiscards = new HashMap<>();
    private Boolean[] targetable;
    private Boolean[] eliminated;
    
    // Used by clone to create a new game state
    private LoveLetterGameState(List<Player> players, Deck deck)
    {
        this.deck = deck;
        this.players = players;
        numberOfPlayers = players.size();
        targetable = new Boolean[players.size()];
        eliminated = new Boolean[players.size()];
    }
    
    public LoveLetterGameState(Game game)
    {
        deck = game.getDeck();
        players = game.getPlayers();
        extraRevealed = game.getExtra();
        numberOfPlayers = players.size();
        targetable = new Boolean[players.size()];
        eliminated = new Boolean[players.size()];
        playerToMove = players.indexOf(game.getCurrentPlayer());
        for (Player p: players)
        {
            List<Card> cards = new ArrayList<>();
            for (Card c: p.getHand())
            {
                cards.add(c);
            }
            playersHands.put(p, cards);
            cards = new ArrayList<>();
            for (Card c: p.getPlayedCards())
            {
                cards.add(c);
            }
            playersDiscards.put(p, cards);
            targetable[players.indexOf(p)] = p.targetable();
            eliminated[players.indexOf(p)] = p.isEliminated();
        }
        totalCards = deck.getFactory().getCards();
        drawPile = deck.getUnknownCards();
        // May do deal here currently game handles that stuff
    }
    
    
    @Override
    public List<GameMove> getMoves() {
        
        Player toMove = players.get(playerToMove);
        List<Card> hand = playersHands.get(toMove);
        List<GameMove> moves = new ArrayList<>();
        
        // Check if the game is over, in which case return an empty list
        if (gameOver())
        {
            return moves;
        }
        
        for (Card c: hand)
        {
            switch(c.getValue())
            {
                case 1:
                    for (int guess = 2; guess < 9; ++guess)
                    {
                        for (int i = 0; i < players.size(); ++i)
                        {
                            if (targetable[i] && !eliminated[i])
                            {
                                moves.add(new LoveLetterMove(c, i, guess, playerToMove));
                            }
                        }
                    }
                    break;
                case 2:
                case 3:
                    for (int i = 0; i < players.size(); ++i)
                        {
                            if (targetable[i] && !eliminated[i])
                            {
                                moves.add(new LoveLetterMove(c, i, 0, playerToMove));
                            }
                        }
                    break;
                case 4:
                    moves.add(new LoveLetterMove(c, playerToMove, 0, playerToMove));
                    break;
                case 5:
                case 6:
                    boolean valid = true;
                    for (Card card : hand) {
                        if (card.getValue() == 7) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        for (int i = 0; i < players.size(); ++i) {
                                if (targetable[i] && !eliminated[i]) {
                                    moves.add(new LoveLetterMove(c, i, 0, playerToMove));
                                }
                        }
                    }
                    break;
                default:
                    moves.add(new LoveLetterMove(c, playerToMove, 0, playerToMove));
                    break;
            }
        }
        return moves;
    }
    
    private boolean eliminate(int target)
    {
        Player p = players.get(target);
        eliminated[target] = true;
        Card card = playersHands.get(p).get(0);
        playersHands.get(p).remove(card);
        playersDiscards.get(p).add(card);
        return eliminated[target];
    }

    @Override
    public void doMove(GameMove move) throws Exception{
        // For some cards, we need the player who is the target
        Player target = players.get(move.getTarget());
        Player owner = players.get(move.getOwner());
        Card card;
        Card playedCard = move.getCard();
        // Remove the card from their hand and place it in their discard
        if (!playersHands.get(owner).contains(playedCard))
            throw new Exception("The card to be played is not in the players hand!");
        playersHands.get(owner).remove(playedCard);
        playersDiscards.get(owner).add(playedCard);
        
        switch(playedCard.getValue())
        {
            case 1:
                if (target != owner)
                {
                    if (playersHands.get(target).get(0).getValue() == move.getGuess())
                    {
                        eliminate(move.getTarget());
                    }
                }
                break;
            case 3:
                if (target != owner)
                {
                    if (playersHands.get(target).get(0).compareTo(playersHands.get(owner).get(0)) < 0)
                    {
                        eliminate(move.getTarget());
                    }
                    else if (playersHands.get(target).get(0).compareTo(playersHands.get(owner).get(0)) > 0)
                    {
                        eliminate(move.getOwner());
                    }
                }
                break;
            case 4:
                targetable[move.getOwner()] = false;
                break;
            case 5:
                card = playersHands.get(target).get(0);
                // Eliminate if they played the princess
                if (card.getValue() == 8)
                {
                    eliminate(move.getTarget());
                }
                else
                {
                    // Remove old card
                    playersHands.get(target).remove(card);
                    playersDiscards.get(target).add(card);
                    // draw a new one
                    if (drawPile.size() > 0)
                    {
                        playersHands.get(target).add(drawPile.get(0));
                        drawPile.remove(0);
                    }
                    else
                    {
                        throw new Exception("No cards in the draw pile");
                    }
                }
                break;
            case 6:
                if (target != owner)
                {
                    // Swap hands with the other player
                    card = playersHands.get(target).get(0);
                    playersHands.get(target).remove(card);
                    playersHands.get(owner).add(card);
                    card = playersHands.get(owner).get(0);
                    playersHands.get(owner).remove(card);
                    playersHands.get(target).add(card);
                }
                break;
            case 7:
                break;
            case 8:
                eliminate(move.getOwner());
                break;
        }
        playerToMove = getNextPlayer(playerToMove);
        
        // Draw a card if the game isn't over yet.
        if (drawPile.size() > 1 && !eliminated[playerToMove])
        {
            card = drawPile.get(0);
            drawPile.remove(0);
            playersHands.get(players.get(playerToMove)).add(card);
        }
    }

    @Override
    public int getResult(Player player) {
        // Return 0 if the player lost, otherwise return 1
        Player winner = null;
        int winnerValue = -1;
        int winnerDiscardValue = -1;
        
        if (eliminated[players.indexOf(player)])
            return 0;
        else
        {
            for (int i = 0; i < players.size(); ++i)
            {
                if (!eliminated[i])
                {
                    int cardValue = playersHands.get(players.get(i)).get(0).getValue();
                    int discardSum = sumCards(playersDiscards.get(players.get(i)));
                    if (cardValue > winnerValue)
                    {
                        winner = players.get(i);
                        winnerValue = cardValue;
                        winnerDiscardValue = discardSum;
                    }
                    else if (cardValue == winnerValue)
                    {
                        if (discardSum > winnerDiscardValue)
                        {
                            winner = players.get(i);
                            winnerValue = cardValue;
                            winnerDiscardValue = discardSum;
                        }
                    }
                }
            }
        }
        if (player == winner)
            return 1;
        else
            return 0;
    }
            
    private int sumCards(List<Card> cards)
    {
        int sum = 0;
        for (Card c: cards)
        {
            sum += c.getValue();
        }
        return sum;
    }

    @Override
    public LoveLetterGameState clone()
    {
        LoveLetterGameState state = new LoveLetterGameState(players, deck);
        state.extraRevealed = extraRevealed;
        state.totalCards = totalCards;
        state.playerToMove = playerToMove;
        // Do a deep copy of each set of values
        for (Player p: players)
        {
            // Keep track of hands
            List<Card> hand = new ArrayList<>();
            List<Card> discard = new ArrayList<>();
            for (Card c: playersHands.get(p))
            {
                hand.add(c);
            }
            for (Card c: playersDiscards.get(p))
            {
                discard.add(c);
            }
            state.playersHands.put(p, hand);
            state.playersDiscards.put(p, discard);
        }
        for (int i = 0; i < players.size(); ++i)
        {
            state.targetable[i] = targetable[i];
            state.eliminated[i] = eliminated[i];
        }
        for (Card c: drawPile)
        {
            state.drawPile.add(c);
        }
        return state;
    }
    
    @Override
    public LoveLetterGameState cloneAndRandomize(Player player) {
        LoveLetterGameState state = clone();
        
        // Keep track of known cards
        List<Card> seenCards = new ArrayList<>();
        // For two player games with extra revealed cards
        seenCards.addAll(extraRevealed);
        for (Card c: playersHands.get(player))
        {
            seenCards.add(c);
        }
        for (List<Card> c: playersDiscards.values())
        {
            seenCards.addAll(c);
        }
        
        List<Card> unseenCards = new ArrayList<>();
        for (Card c: totalCards)
        {
            if (!seenCards.contains(c))
            {
                unseenCards.add(c);
            }
        }
        
        Collections.shuffle(unseenCards);
        for (Player p: players)
        {
            if (p != player && !eliminated[players.indexOf(p)])
            {
                List<Card> temp = new ArrayList<>();
                temp.add(unseenCards.get(0));
                state.playersHands.put(p, temp);
                // Remove the card from the unseen cards list
                unseenCards.remove(0);
            }
        }
        drawPile = unseenCards;
        return state;
    }
    
    @Override
    public int getNextPlayer(int p)
    {
        // The +1 are because we need to move to the next player
        // The index starts at 0
        int next = ((p+1) % numberOfPlayers);
        while (next != p && eliminated[next])
            next = ((next+1) % numberOfPlayers);
        // Set the next player to targetable if they are not currently
        // On a player's turn, they become retargetable
        if (!targetable[next])
            targetable[next] = true;
        return next;
    }

    /**
     * Pick a random move to make
     * @return The move chosen
     */
    @Override
    public GameMove getSimulationMove() {
        List<GameMove> moves = getMoves();
        return moves.get(ThreadLocalRandom.current().nextInt(0, moves.size()));
    }

    @Override
    public Player getPlayerToMove() {
        return players.get(playerToMove);
    }
    
    private boolean gameOver()
    {
        // If the draw pile has only one card (aka. we only have the one set aside card left) the game is over
        if (drawPile.size() <= 1)
            // if they only have one card in their hand then they have moved.
            if (playersHands.get(players.get(playerToMove)).size() == 1)
                return true;
        if (drawPile.isEmpty())
        {
            System.out.println("The draw pile is empty so the game must be over!");
            System.out.println("The players hand sizes are: ");
            for (int i = 0; i < players.size(); ++i)
            {
                System.out.print(players.get(i).getName() + ":");
                System.out.println(playersHands.get(players.get(i)).size());
            }
            System.out.println("The player to move is: " + players.get(playerToMove).getName());
            return true;
        }
        
        // Find out how many players have been eliminated
        int numEliminated = 0;
        for (int i = 0; i < players.size(); ++i)
        {
            if (eliminated[i])
                ++numEliminated;
        }
        
        // If there is only one player left, then the game is over
        return numEliminated >= (players.size()-1);
    }
}
