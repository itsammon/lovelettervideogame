/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameStates;

import ammonclegg.lovelettercore.Card;

/**
 *
 * @author ammon
 */
public interface GameMove {
    
    // Used to allow game moves
    public Card getCard();
    public int getTarget();
    public int getGuess();
    public int getOwner();
}
