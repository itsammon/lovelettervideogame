/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameStates;

import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.Player;
import java.util.List;

/**
 *
 * @author ammon
 */
public abstract class GameState 
{
    protected int numberOfPlayers = 2;
    // Indexes start at 0
    protected int playerToMove = 0;
    
    public abstract List<GameMove> getMoves();
    public void doMove(GameMove move) throws Exception
    {
        playerToMove = getNextPlayer(playerToMove);
    }
    public abstract int getResult(Player player);
    
    public int getNextPlayer(int p)
    {
        return (p % numberOfPlayers);
    }
    
    public abstract Player getPlayerToMove();
    
    public GameState cloneAndRandomize(Player observer) throws CloneNotSupportedException
    {
        return clone();
    }

    @Override
    public GameState clone() throws CloneNotSupportedException
    {
        GameState state = (GameState) super.clone();
        state.playerToMove = playerToMove;
        return state;
    }
    public abstract GameMove getSimulationMove();
}
