/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AI;

import GameStates.GameMove;
import GameStates.GameState;
import GameStates.LoveLetterGameState;
import MonteCarlo.ISTreeNode;
import MonteCarlo.TreeNode;
import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.IllegalMoveException;
import ammonclegg.lovelettercore.Player;
import static java.lang.Thread.sleep;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Design based on an implementation by 
 * Peter Cowling, Edward Powley, Daniel Whitehouse (University of York, UK) September 2012 - August 2013.
 * @author ammon
 */
public class MonteCarloSearchTreePlayer  extends Player implements Runnable{
    private int iterations;
    private GameMove bestMove;
    private Game game;
    
    public MonteCarloSearchTreePlayer(String name, int iterations)
    {
        super(name);
        this.iterations = iterations;
    }
    
    public void setGame(Game game)
    {
        this.game = game;
    }
    
    public GameMove search(GameState rootState, int iterations, boolean verbose) throws CloneNotSupportedException
    {
        TreeNode rootNode = new ISTreeNode();
        TreeNode node;
        for (int i = 0; i < iterations; ++i)
        {
            node = rootNode;
            GameState state = rootState.cloneAndRandomize(rootState.getPlayerToMove());
            
            // Select
            while (!state.getMoves().isEmpty() && node.getUntriedMoves(state.getMoves()).isEmpty())
            {
                node = node.selectChild(state.getMoves(), 0.7);
                try {
                    state.doMove(node.getMove());
                } catch (Exception ex) {
                    Logger.getLogger(MonteCarloSearchTreePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            // Expand
            List<GameMove> untriedMoves = node.getUntriedMoves(state.getMoves());
            if (!untriedMoves.isEmpty())
            {
                GameMove move = node.selectUntriedMove(untriedMoves);
                Player player = state.getPlayerToMove();
                try
                {
                    state.doMove(move);
                }
                catch (Exception ex)
                {
                    Logger.getLogger(MonteCarloSearchTreePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
                node = node.addChild(move, player);
            }
            
            // Simulate
            while (!state.getMoves().isEmpty())
            {
                try {
                    state.doMove(state.getSimulationMove());
                } catch (Exception ex) {
                    Logger.getLogger(MonteCarloSearchTreePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            // Backpropagate
            while (node != null)
            {
                node.update(state);
                node = node.getParent();
            }
        }
        
        if (verbose)
        {
            System.out.println(rootNode.treeToString(0));
        }
        
        return rootNode.getBestMove();
    }

    @Override
    public Player requestTarget(List<Player> targets) {
        //System.out.println("Player " + getName() + " targets " + game.getPlayers().get(bestMove.getTarget()).getName());
        return game.getPlayers().get(bestMove.getTarget());
    }

    @Override
    public int requestGuess(List<Integer> options, List<Card> visibleCards) {
        //System.out.println("Player " + getName() + " guesses " + bestMove.getGuess());
        return bestMove.getGuess();
    }
    
    @Override
    public void run() {
        while (!isEliminated())
        {
            if (isTurn())
            { 
                // Get the current state of the game into state form
                GameState currGameState = new LoveLetterGameState(game);
                try
                {
                    bestMove = search(currGameState, iterations, false);
                    play(bestMove.getCard());
                } catch (IllegalMoveException | CloneNotSupportedException ex) {
                    Logger.getLogger(MonteCarloSearchTreePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }   
            else
            {
                try {
                    sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TestAiPlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
