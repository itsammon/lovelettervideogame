/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AI;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.IllegalMoveException;
import ammonclegg.lovelettercore.Player;
import static java.lang.Thread.sleep;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Completely Random Player
 * @author ammon
 */
public class RandomPlayer extends Player implements Runnable{

    public RandomPlayer(String name)
    {
        super(name);
    }
    
    @Override
    public Player requestTarget(List<Player> targets) {
        Player choice = targets.get(ThreadLocalRandom.current().nextInt(0, targets.size()));
        //System.out.println("Player " + getName() + " targets " + choice.getName());
        return choice;
    }

    @Override
    public int requestGuess(List<Integer> options, List<Card> visibleCards) {
        int choice = options.get(ThreadLocalRandom.current().nextInt(0, options.size()));
        //System.out.println("Player " + getName() + " guesses " + choice);
        return choice;
    }

    @Override
    public void run() {
        boolean illegalMove;
        Card choice;
        while (!isEliminated())
        {
            if (isTurn())
            {
                illegalMove = true;
                while (illegalMove)
                {
                    try {         
                        choice = getHand().get(ThreadLocalRandom.current().nextInt(0,getHand().size()));
                        play(choice);
                        illegalMove = false;
                    } catch (IllegalMoveException ex) {
                        illegalMove = true;
                    }
                }
            }   
            else
            {
                try {
                    sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RandomPlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
