/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AI;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.IllegalMoveException;
import ammonclegg.lovelettercore.Player;
import static java.lang.Thread.sleep;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class TestAiPlayer extends Player implements Runnable{
    private Map<Integer, Integer> numEachCard;
    
    public TestAiPlayer(String name, Map<Integer, Integer> numEachCard)
    {
        super(name);
        this.numEachCard = numEachCard;
    }

    @Override
    public Player requestTarget(List<Player> targets) {
        Player choice = targets.get(ThreadLocalRandom.current().nextInt(0, targets.size()));
        
        // Don't target ourselves when we hold the princess unless we are the only target
        while (getHand().get(0).getValue() == 8 && choice.equals(this))
        {
            if (targets.size() == 1)
                break;
            else
                choice = targets.get(ThreadLocalRandom.current().nextInt(0, targets.size()));
        }
        
        //System.out.println("Player " + getName() + " targets " + choice.getName());
        return choice;
    }

    @Override
    public int requestGuess(List<Integer> options, List<Card> visibleCards) {
        Map<Integer, Integer> cardsLeft = new HashMap<>();
        cardsLeft.putAll(numEachCard);
        int num;
        
        // Reduce the number of each card left by what has been played
        for (Card c: visibleCards)
        {
            num = cardsLeft.get(c.getValue());
            --num;
            cardsLeft.put(c.getValue(), num);
        }
        
        // Determine the card with the highest value with the most left
        int choice = options.get(0);
        num = cardsLeft.get(choice);
        for (int i: options)
        {    
            if (cardsLeft.get(i) >= num && i >= choice)
            {
                choice = i;
                num = cardsLeft.get(i);
            }
        }
        //System.out.println("Player " + getName() + " guesses " + choice);
        return choice;
    }

    @Override
    public void run() {
        int choice;
        while (!isEliminated())
        {
            if (isTurn())
            { 
                // Pick which card to play, if possible play the lower valued card
                if (getHand().get(0).compareTo(getHand().get(1)) <= 0) {
                    choice = 0;
                } else {
                    choice = 1;
                }
                try {
                    play(getHand().get(choice));
                } catch (IllegalMoveException ex) {
                    try {
                        //System.out.println("Player " + getName() + " has " 
                        //+ getHand().get(0).getName() + " and " 
                        //+ getHand().get(1).getName() + " in his hand.");
                        if (choice == 0) {
                            play(getHand().get(1));
                        } else {
                            play(getHand().get(0));
                        }
                    } catch (IllegalMoveException e) {
                        System.out.println("Both cards are illegal!");
                    }
                }
            }   
            else
            {
                try {
                    sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TestAiPlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
