/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.lovelettercore;

import java.awt.Image;

/**
 *
 * @author ammon
 */
public abstract class Card implements Comparable<Card>{
    private String imageFileName;
    protected Player target;
    protected Player holder;
    protected Game game;
    
    public abstract int getValue();
    public abstract String getName();
    public abstract String getDescription();
    public String getImageFileName() { return imageFileName; }
    public abstract Image getImage();
    public abstract void doAction() throws IllegalMoveException;
    public void discard() { /* Do nothing if a card is discarded normally */ }
    public void setTarget(Player t) { target = t; }
    public Player getTarget() { return target; }
    public void setHolder(Player o) { holder = o; }
    public Player getHolder() { return holder; }
    
    /**
     * Compare to function allowing a card to be compared
     * Needs overwritten if cards can be greater than others in special
     * circumstances, works for base game
     * @param c The card to compare this one to.
     * @return An integer: 1 indicating this card was greater than that card
     *  -1 indicating this card is less than the other, and 0 indicating equality
     */
    @Override
    public int compareTo(Card c)
    {
        if (c.getValue() == this.getValue())
            return 0;
        else
            return getValue() > c.getValue() ? 1 : -1;
    }
    
}
