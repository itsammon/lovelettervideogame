/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.lovelettercore;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO: Add in locks
 * @author ammon
 */
public class Game {
    private List<Player> players = new ArrayList<>();
    private List<Card> extraRevealed = new ArrayList<>();
    private int currentPlayer;
    // TODO: Add in support for observers
    private Deck deck;
    // TODO: Add in ruleset support
    
    public Game()
    {
        
    }
    
    public void setDeck(Deck deck)
    {
        this.deck = deck;
    }
    
    public Deck getDeck()
    {
        return deck;
    }
   
    public void addPlayer(Player player)
    {
        // Add check to manage if the player can join. Need to check ruleset.
        players.add(player);
    }
    
    public List<Player> getPlayers()
    {
        return players;
    }
    
    public void removePlayer(Player player)
    {
        players.remove(player);
    }
    
    public Player getCurrentPlayer()
    {
        return players.get(currentPlayer);
    }
    
    public List<Card> getVisibleCards(Player player)
    {
        List<Card> visible = new ArrayList<>();
        for (Player p: players)
        {
            visible.addAll(p.getPlayedCards());
        }
        visible.addAll(extraRevealed);
        visible.addAll(player.getHand());
        return visible;
    }
    
    public List<Player> getTargets(boolean includeSelf)
    {
        List<Player> targets = new ArrayList<>();
        for (int i = 0; i < players.size(); ++i)
        {
            if (players.get(i).targetable() && !players.get(i).isEliminated())
            {
                // If we are not the currentPlayer or if we include ourself
                // Add us to the list
                if (i != currentPlayer || includeSelf)
                {
                    targets.add(players.get(i));
                }
            }
        }
        return targets;
    }
    
    public void reset()
    {
        // Reset players hands
        for (Player p: players)
        {
            p.reset();
        }
        extraRevealed = new ArrayList<>();
        currentPlayer = ThreadLocalRandom.current().nextInt(0,players.size());
        deck.shuffle();
    }
    
    public List<Card> getExtra()
    {
        List<Card> temp = new ArrayList<>();
        temp.addAll(extraRevealed);
        return temp;
    }
    
    public Player startGame()
    {
        // If there are only two players, get the revealed cards
        if (players.size() == 2)
        {
            for (int i = 0; i < 3; ++i)
            {
                extraRevealed.add(deck.drawOne());
            }
        }
        
        // Each player draws a card
        for (Player p: players)
        {
            deck.drawOne(p);
        }
        
        while (!roundOver())
        {
            if(!players.get(currentPlayer).isEliminated())
            {
                deck.drawOne(players.get(currentPlayer));
                players.get(currentPlayer).turn();
                while(players.get(currentPlayer).isTurn())
                {
                    try {
                        sleep(100);
                    } catch (InterruptedException ex) {
                        break;
                    }
                }
            }
            currentPlayer = (currentPlayer+1) % players.size();
        }
        
        int winner = roundEnd();
        System.out.println("The winner was " + players.get(winner).getName());
        
        // End the threads for each player
        for (Player p: players)
        {
            p.eliminate();
        }
        
        // Return a reference to the winner
        return players.get(winner);
    }
    
    public boolean roundOver()
    {   
        int numPlayers = 0;
        if (deck.isEmpty())
        {
            return true;
        }
        
        for (Player p: players)
        {
            if (!p.isEliminated())
            {
                numPlayers += 1;
            }
        }
        return numPlayers <= 1;
    }
    
    public int roundEnd()
    {
        int numPlayers = 0;
        Player winner = null;
        for (Player p: players)
        {
            if (!p.isEliminated())
            {
                numPlayers += 1;
            }
        }
        
        if (numPlayers > 1)
        {
            for (Player p: players)
            {
                if (!p.isEliminated())
                {
                    if (winner == null)
                    {
                        winner = p;
                    }
                    else if (p.compareHands(winner) > 0)
                    {
                        winner = p;
                    }
                    else if (p.compareHands(winner) == 0)
                    {
                        // Handle ties
                        if (p.getTotalPlayedValue() > winner.getTotalPlayedValue())
                        {
                            winner = p;
                        }
                    }
                }
            }
        }
        else
        {
            for (Player p: players)
            {
                if (!p.isEliminated())
                {
                    return players.indexOf(p);
                }
            }
        }
        return players.indexOf(winner);
    }
}
