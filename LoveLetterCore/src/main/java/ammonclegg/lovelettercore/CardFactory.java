/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.lovelettercore;

import java.util.Map;
import java.util.Stack;

/**
 *
 * @author ammon
 */
public interface CardFactory {
    public void setGame(Game game);
    public Stack<Card> getDeck();
    public Stack<Card> getCards();
    public Map<Integer, Integer> getNumEachCard();
}
