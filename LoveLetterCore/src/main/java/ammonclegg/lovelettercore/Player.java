/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.lovelettercore;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author ammon
 */
public abstract class Player implements Runnable{
    private final List<Card> hand;
    private final List<Card> playedCards;
    private boolean eliminated = false;
    private boolean targetable = false;
    private boolean isTurn = false;
    protected boolean takingTurn = false;
    private String name;
    private Lock myLock = new ReentrantLock();
    
    public Player(String name)
    {
        hand = new CopyOnWriteArrayList<>();
        playedCards = new CopyOnWriteArrayList<>();
        this.name = name;
    }
    
    public void setName(String name)
    {
        myLock.lock();
        try
        {
            this.name = name;
        }
        finally
        {
            myLock.unlock();
        }
    }
    public String getName()
    {
        return name;
    }
    
    public void addCard(Card c)
    {
        myLock.lock();
        try
        {
            hand.add(c);
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public List<Card> getHand()
    {
        return hand;
    }
    
    public List<Card> getPlayedCards()
    {
        return playedCards;
    }
    
    public void reset()
    {
        myLock.lock();
        try
        {
            hand.clear();
            playedCards.clear();
            eliminated = false;
            targetable = true;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public int getTotalPlayedValue()
    {
        int sum = 0;
        myLock.lock();
        try
        {
            for (Card c: playedCards)
            {
                sum += c.getValue();
            }
        }
        finally
        {
            myLock.unlock();
        }
        return sum;
    }
    
    public boolean isEliminated()
    {
        return eliminated;
    }
    
    public void eliminate()
    {
        myLock.lock();
        try
        {
        for (Card c: hand)
        {
            playedCards.add(c);
        }
        hand.clear();
        eliminated = true;
        
        System.out.println("Player " + name + " was eliminated!");
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public boolean targetable()
    {
        return targetable;
    }
    
    public void makeNotTargetable()
    {
        myLock.lock();
        try
        {
            targetable = false;}
        finally
        {
            myLock.unlock();
        }
    }
    
    public boolean isTurn()
    {
        return isTurn;
    }
    
    public void turn()
    {
        myLock.lock();
        try
        {
        targetable = true;
        isTurn = true;
        
        // Testing purposes
        /*
                System.out.println("Player " + getName() + " has " 
                        + getHand().get(0).getName() + " and " 
                        + getHand().get(1).getName() + " in his hand.");
*/
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public void discard()
    {
        myLock.lock();
        try
        {
        Card discarded = hand.remove(0);
        playedCards.add(discarded);
        discarded.discard();
        System.out.println("Player " + name + " discarded " + discarded.getName());
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public void play(Card choice) throws IllegalMoveException
    {
        
        if (isTurn)
        {
        myLock.lock();
        try
        {
            try
            {
                hand.remove(choice);
                System.out.println("Player " + name + " played " + choice.getName());
                choice.doAction();
                playedCards.add(choice);
                isTurn = false;
            }
            catch(IllegalMoveException ex)
            {
                System.out.println("Player " + name + " could not play " + choice.getName());
                System.out.println("Illegal Move");
                hand.add(choice);
                throw ex;
            }
        }
        finally
        {
            myLock.unlock();
        }
        }
    }
    
    public abstract Player requestTarget(List<Player> targets) ;
    public abstract int requestGuess(List<Integer> options, List<Card> visibleCards);
    
    public void swapHands(Player p)
    {
        myLock.lock();
        p.myLock.lock();
        try
        {
            List<Card> tmpList = new CopyOnWriteArrayList<>(hand);
            hand.clear();
            hand.addAll(p.hand);
            p.hand.clear();
            p.hand.addAll(tmpList);
            // Change ownership of cards
            for (Card c : hand) {
                c.setHolder(this);
            }
            for (Card c : p.hand) {
                c.setHolder(p);
            }
        }
        finally
        {
            p.myLock.unlock();
            myLock.unlock();
        }
    }
    
    public int compareHands(Player p)
    {
        myLock.lock();
        try
        {
        // Player should have only one card in their hand at any given moment
        return hand.get(0).compareTo(p.hand.get(0));
        }
        finally
        {
            myLock.unlock();
        }
    }

    @Override
    public abstract void run();

}
