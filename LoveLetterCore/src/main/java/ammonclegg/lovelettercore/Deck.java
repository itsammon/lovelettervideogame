/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ammonclegg.lovelettercore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author ammon
 */
public class Deck {
    private Stack<Card> deck;
    private CardFactory factory;
    private Lock myLock = new ReentrantLock();
    private Card setAside;
    
    public Deck(CardFactory factory)
    {
        this.factory = factory;
    }
    
    public Card getSetAside()
    {
        return setAside;
    }
    
    public void setFactory(CardFactory factory)
    {
        myLock.lock();
        try
        {
        this.factory = factory;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public CardFactory getFactory()
    {
        return factory;
    }
    
    public void shuffle()
    {
        myLock.lock();
        try
        {
            deck = new Stack<>();
            deck.addAll(this.factory.getDeck());
            Collections.shuffle(deck);
            setAside = deck.pop();
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    // Used for two player games to handle the extra revealed cards
    public Card drawOne()
    {
        myLock.lock();
        try
        {
        if (deck != null)
        {
            Card temp;
            if (deck.size() > 0)
            {
                temp = deck.pop();
            }
            else
            {
                // If there was no card in the deck (Mostly for prince)
                temp = setAside;
                setAside = null;
            }
            return temp;
        }
        else
        {
            // TODO: Handle the lack of a deck
        }
        }
        finally
        {
            myLock.unlock();
        }
        return null;
    }
    
    public Card drawOne(Player p)
    {
        myLock.lock();
        try
        {
        if (deck != null)
        {
            Card temp;
            if (deck.size() > 0)
            {
                temp = deck.pop();
            }
            else
            {
                // If there was no card in the deck (Mostly for prince)
                temp = setAside;
                setAside = null;
            }
            temp.setHolder(p);
            p.addCard(temp);
        }
        else
        {
            // Handle the lack of a deck
        }
        }
        finally
        {
            myLock.unlock();
        }
        return null;
    }
    
    public boolean isEmpty()
    {
        return deck.isEmpty();
    }
    
    public List<Card> getUnknownCards()
    {
        List<Card> temp = new ArrayList<>();
        for (Card c: deck)
        {
            temp.add(c);
        }
        temp.add(setAside);
        Collections.shuffle(temp);
        return temp;
    }
}
