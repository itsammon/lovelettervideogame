/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import AI.MonteCarloSearchTreePlayer;
import AI.RandomPlayer;
import AI.TestAiPlayer;
import OriginalGame.OriginalFactory;
import ammonclegg.lovelettercore.CardFactory;
import ammonclegg.lovelettercore.Deck;
import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.Player;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class Main {
    private static List<Player> players = new ArrayList<>();
    private static List<Thread> threads;
    private static int iterations = 100;
    private static final int NUM_GAMES = 1000;
    private static final int DEFAULT_ITERATIONS = 1000;
    private static Game game;
    private static Deck deck;
    private static CardFactory factory;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        //vsMode();
        //runIterationTests();
        multiplayerTests();
    }
    
    private static void playGame()
    {
        Scanner scan = new Scanner(System.in);
        int numRandom;
        int numTest;
        int numMonte;
        char play = 'y';
        System.out.println("Please enter the number of random players: ");
        numRandom = scan.nextInt();
        
        System.out.println("Please enter the number of test players: ");
        numTest = scan.nextInt();
        
        System.out.println("Please enter the number of monte players: ");
        numMonte = scan.nextInt();
        
        while ((numRandom+numTest+numMonte) <= 2 || (numRandom+numTest+numMonte) > 4 || numRandom < 0 || numTest < 0 || numMonte < 0)
        {
            System.out.println("There is an invalid number of players!");
            System.out.println("Please enter the number of random players: ");
            numRandom = scan.nextInt();
        
            System.out.println("Please enter the number of test players: ");
            numTest = scan.nextInt();
        
            System.out.println("Please enter the number of monte players: ");
            numMonte = scan.nextInt();
        }
        
        System.out.println("Please enter the number of iterations for the monte players: ");
        iterations = scan.nextInt();
        
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        createRandom(numRandom);
        createTest(numTest);
        createMonte(numMonte);
        
        addPlayers();
        scan.nextLine();
        
        while(play == 'y' || play == 'Y')
        {
            startGame();
        
            System.out.println("Do you wish to play again? ");
            String resp = scan.nextLine();
            play = resp.charAt(0);
        }
    }
    
    
    private static void createRandom(int numPlayers)
    {
        Player player;
        for (int i = 0; i < numPlayers; i++)
        {
            player = new RandomPlayer("Random" + (i+1));
            players.add(player);
        }
    }
    
    private static void createTest(int numPlayers)
    {
        Player player;
        for (int i = 0; i < numPlayers; i++)
        {
            player = new TestAiPlayer("Test" + (i+1), factory.getNumEachCard());
            players.add(player);
        }
    }
    
    private static void createMonte(int numPlayers)
    {
        MonteCarloSearchTreePlayer player;
        for (int i = 0; i < numPlayers; i++)
        {
            player = new MonteCarloSearchTreePlayer("Monte" + (i+1), iterations);
            players.add(player);
            player.setGame(game);
        }
    }
    
    private static void addPlayers()
    {
        for (Player p: players)
        {
            game.addPlayer(p);
        }
    }
    
    private static void createThreads()
    {
        threads = new ArrayList<>();
        for (Player p: players)
        {
            threads.add(new Thread(p, p.getName()));
        }
    }
    
    private static Player startGame()
    {
        Player winner;
        game.reset();
        createThreads();
        // Start the threads
        for (Thread t: threads)
        {
            t.start();
        }
        winner = game.startGame();
        // Wait to make sure threads complete execution.
        try {
            for (Thread t: threads)
                t.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return winner;
    }
    
    //TODO: Write testing framework for the data collection
    
    private static void vsMode()
    {
        try{
            PrintWriter writer = new PrintWriter("2PlayerResults.txt", "UTF-8");
            writer.println("The win rate for RandvsRand is " + playRandomVsRandom());
            writer.println("The win rate for RandvsHuman is " + playRandomVsHuman());
            writer.println("The win rate for RandvsMonte is " + playRandomVsMonte());
            writer.println("The win rate for HumanvsHuman is " + playHumanVsHuman());
            writer.println("The win rate for HumanvsMonte is " + playHumanVsMonte());
            writer.println("The win rate for MontevsMonte is " + playMonteVsMonte());
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
    
    private static void multiplayerTests()
    {
        try{
            PrintWriter writer = new PrintWriter("MultiplayerResults.txt", "UTF-8");
            writer.println("The win rate for 2Humans is " + playMulti(2, 0));
            writer.println("The win rate for 3Humans is " + playMulti(3, 0));
            writer.println("The win rate for 2Monte is " + playMulti(0, 2));
            writer.println("The win rate for 3Monte is " + playMulti(0, 3));
           writer.println("The win rate for 1Human1Monte is " + playMulti(1,1));
           writer.println("The win rate for 2Human1Monte is " + playMulti(2,1));
            writer.println("The win rate for 1Human2Monte is " + playMulti(1,2));
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
    
    private static double playMulti(int human, int monte)
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        iterations = 3000;
        
        createMonte(1);
        createTest(human);
        createMonte(monte);
        addPlayers();
        
        return runTest();
    }
    
    private static void runIterationTests()
    {
        try{
            PrintWriter writer = new PrintWriter("iterationTests.txt", "UTF-8");
            writer.println("Iterations\tWinRate");
            for (int iter = 1; iter < 100000; iter *= 5)
            {
                writer.println(iter + "\t" + monteIterTest(iter));
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
    
    private static double playRandomVsRandom()
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        
        createRandom(2);
        addPlayers();
        
        return runTest();
    }
    
    private static double playRandomVsHuman()
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        
        createRandom(1);
        createTest(1);
        addPlayers();
        
        return runTest();
    }
    
    private static double playRandomVsMonte()
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        iterations = DEFAULT_ITERATIONS;
        
        createRandom(1);
        createMonte(1);
        addPlayers();
        
        return runTest();
    }
    
    private static double playHumanVsHuman()
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        iterations = DEFAULT_ITERATIONS;
        
        createTest(2);
        addPlayers();
        
        return runTest();
    }
    
    private static double playHumanVsMonte()
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        iterations = DEFAULT_ITERATIONS;
        
        createTest(1);
        createMonte(1);
        addPlayers();
        
        return runTest();
    }
    
    private static double playMonteVsMonte()
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        iterations = DEFAULT_ITERATIONS;
        
        createMonte(2);
        addPlayers();
        
        return runTest();
    }
    
    private static double monteIterTest(int iter)
    {
        // Setup game for test
        game = new Game();
        factory = new OriginalFactory(game);
        deck = new Deck(factory);
        game.setDeck(deck);
        players = new ArrayList<>();
        iterations = iter;
        
        createMonte(1);
        createTest(1);
        addPlayers();
        
        return runTest();
    }
    
    private static double runTest()
    {
        Player winner;
        HashMap<Player, Integer> wins = new HashMap<>();
        for (Player p: players)
        {
            wins.put(p, 0);
        }
        
        for (int i = 0; i < NUM_GAMES; ++i)
        {
            System.out.println("Game number" + (i+1));
            winner = startGame();
            int numWins = wins.get(winner)+1;
            wins.replace(winner, numWins);
        }
        
        System.out.print(players.get(0));
        
        return (double) wins.get(players.get(0)) / (double) NUM_GAMES;
    }
}
