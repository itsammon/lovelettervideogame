/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OriginalGame;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.IllegalMoveException;
import ammonclegg.lovelettercore.Player;
import java.awt.Image;
import java.util.List;

/**
 *
 * @author ammon
 */
public class Prince extends Card {
    
    public Prince (Game game)
    {
        this.game = game;
    }

    @Override
    public int getValue() {
        return 5;
    }

    @Override
    public String getName() {
        return "Prince";
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getImage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doAction() throws IllegalMoveException {
        // Handle the issue of holding the countess
        List<Card> hand = holder.getHand();
        //System.out.println("The card remaining in the hand of the player is " + hand.get(0).getName());
        if (hand.get(0).getName().equals("Countess"))
        {
            throw new IllegalMoveException("The countess cannot be played when the prince or king is in your hand!");
        }
        
        List<Player> targetList = game.getTargets(true);
        // If there are valid targets handle the swap
        if (!targetList.isEmpty())
        {
            target = holder.requestTarget(targetList);
            target.discard();
            if (!target.isEliminated())
            {
                game.getDeck().drawOne(target);
            }
        }
    }
    
}
