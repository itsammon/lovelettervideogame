/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OriginalGame;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.IllegalMoveException;
import java.awt.Image;
import java.util.List;

/**
 *
 * @author ammon
 */
public class Countess extends Card {
    
    @Override
    public int getValue() {
        return 7;
    }

    @Override
    public String getName() {
        return "Countess";
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getImage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doAction() throws IllegalMoveException {
        // The countess does nothing when played
    }
}
