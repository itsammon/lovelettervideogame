/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OriginalGame;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.CardFactory;
import ammonclegg.lovelettercore.Game;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author ammon
 */
public class OriginalFactory implements CardFactory{
    private static Stack<Card> deck;
    private static int currCardID = 0;
    private static final int NUM_GUARDS = 5;
    private static final int NUM_PRIESTS = 2;
    private static final int NUM_BARONS = 2;
    private static final int NUM_HANDMAIDS = 2;
    private static final int NUM_PRINCES = 2;
    private static final int NUM_KINGS = 1;
    private static final int NUM_COUNTESSES = 1;
    private static final int NUM_PRINCESSES = 1;
    
    public Game game;
    
    public OriginalFactory(Game game)
    {
        this.game = game;
    }
    
    private int getID()
    {
        currCardID += 1;
        return currCardID;
    }

    @Override
    public Stack<Card> getDeck() {
        deck = new Stack<>();
        
        // Create the guards
        for (int i = 0; i < NUM_GUARDS; ++i)
        {
            deck.add(new Guard(game));
        }
        for (int i = 0; i < NUM_PRIESTS; ++i)
        {
            deck.add(new Priest());
        }
        for (int i = 0; i < NUM_BARONS; ++i)
        {
            deck.add(new Baron(game));
        }
        for (int i = 0; i < NUM_HANDMAIDS; ++i)
        {
            deck.add(new Handmaid());
        }
        for (int i = 0; i < NUM_PRINCES; ++i)
        {
            deck.add(new Prince(game));
        }
        for (int i = 0; i < NUM_KINGS; ++i)
        {
            deck.add(new King(game));
        }
        for (int i = 0; i < NUM_COUNTESSES; ++i)
        {
            deck.add(new Countess());
        }
        for (int i = 0; i < NUM_PRINCESSES; ++i)
        {
            deck.add(new Princess());
        }
        return deck;
    }
    
    public Stack<Card> getCards()
    {
        return deck;
    }
    

    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public Map<Integer, Integer> getNumEachCard() {
        Map<Integer, Integer> temp = new HashMap<>();
        temp.put(1, NUM_GUARDS);
        temp.put(2, NUM_PRIESTS);
        temp.put(3, NUM_BARONS);
        temp.put(4, NUM_HANDMAIDS);
        temp.put(5, NUM_PRINCES);
        temp.put(6, NUM_KINGS);
        temp.put(7, NUM_COUNTESSES);
        temp.put(8, NUM_PRINCESSES);
        return temp;
    }
    
    
}
