/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OriginalGame;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.Player;
import java.awt.Image;
import java.util.List;

/**
 *
 * @author ammon
 */
public class Baron extends Card {
    
    public Baron(Game game)
    {
        this.game = game;
    }

    @Override
    public int getValue() {
        return 3;
    }

    @Override
    public String getName() {
        return "Baron";
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getImage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doAction() {
        List<Player> targetList = game.getTargets(false);
        // If there are valid targets handle the swap
        if (!targetList.isEmpty())
        {
            target = holder.requestTarget(targetList);
            int compare = holder.compareHands(target);
            if (compare < 0)
            {
                holder.eliminate();
            }
            else if (compare > 0)
            {
                target.eliminate();
            }
        }
    }
    
}
