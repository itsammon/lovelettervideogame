/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OriginalGame;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.Game;
import ammonclegg.lovelettercore.Player;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ammon
 */
public class Guard extends Card {
    
    public Guard(Game game)
    {
        this.game = game;
    }

    @Override
    public int getValue() {
        return 1;
    }

    @Override
    public String getName() {
        return "Guard";
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getImage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doAction() {
        List<Player> targetList = game.getTargets(false);
        List<Card> visible = game.getVisibleCards(game.getCurrentPlayer());
        // If there are valid targets handle the swap
        if (!targetList.isEmpty())
        {
            target = holder.requestTarget(targetList);
            List<Integer> options = new ArrayList<>();
            for (int i = 2; i < 9; ++i)
            {
                options.add(i);
            }
            int guess = holder.requestGuess(options, visible);
            
            if (target.getHand().get(0).getValue() == guess)
            {
                target.eliminate();
            }
        }
    }
    
}
