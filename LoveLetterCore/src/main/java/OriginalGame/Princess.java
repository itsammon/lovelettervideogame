/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OriginalGame;

import ammonclegg.lovelettercore.Card;
import ammonclegg.lovelettercore.Player;
import java.awt.Image;

/**
 *
 * @author ammon
 */
public class Princess extends Card{

    @Override
    public int getValue() {
        return 8;
    }

    @Override
    public String getName() {
        return "Princess";
    }

    @Override
    public Image getImage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doAction() {
        holder.eliminate();
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void discard() {
        holder.eliminate();
    }
    
    @Override
    public void setTarget(Player t)
    {
        target = null;
    }
    
}
